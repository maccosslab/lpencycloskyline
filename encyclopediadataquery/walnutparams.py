import luigi
from lakitu.aws.awstask import FilePathParameter


class WalnutParams(luigi.Config):
    precursor_ppm_error = luigi.FloatParameter(default=10.0,
                                               description='Walnut: precursor ppm error')
    fragment_ppm_error = luigi.FloatParameter(default=10.0,
                                              description='Walnut: fragment ppm error')
    fasta_file = FilePathParameter(description='FASTA file for EncyclopeDIA search')
