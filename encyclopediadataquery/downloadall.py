import luigi
from lakitu.aws.awstask import FilePathParameter
from lakitu.aws.ecs import ECSTask
from luigi.tools import deps

from downloadoutputs import DownloadOutputs
from util import SpecificTaskParameter


class DownloadAll(luigi.WrapperTask):
    seed_task = SpecificTaskParameter()
    output_path = FilePathParameter()

    def requires(self):
        upstream = deps.find_deps(self.seed_task, None)
        for t in upstream:
            if not isinstance(t, ECSTask):
                continue
            if len(t.downloadable_outputs) > 0:
                yield DownloadOutputs(task_object=t, output_path=self.output_path)


if __name__ == '__main__':
    luigi.run()
