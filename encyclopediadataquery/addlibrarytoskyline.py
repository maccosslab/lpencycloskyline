import os

import luigi
import luigi.contrib.s3 as s3
from lakitu.aws import s3helpers
from lakitu.aws.awstask import FilePathParameter
from lakitu.aws.ecs import ECSTask
from lakituapi.v0_0.pipeline import LakituAwsConfig
from luigi.util import inherits

from buildtooldirectory import BuildToolDirectory
from encyclopediabuildmergedlib import EncyclopediaBuildMergedLib
from msconvert import get_log_config
from uploadinputfiletos3 import UploadInputFileToS3


@inherits(EncyclopediaBuildMergedLib)
class AddLibraryToSkyline(ECSTask):
    skyline_template = FilePathParameter(default="s3://jegertso.lakitu/skyline_templates/lakitu_template.sky")

    container_name = "skyline"
    cluster_name = LakituAwsConfig.windows_cluster_name
    task_def = {
        'family': 'skyline_testbuild_20180221_rc0_addlib',
        'taskRoleArn': LakituAwsConfig.task_arn,
        'volumes': [],
        'containerDefinitions': [
            {
                'name': container_name,
                'image': 'atkeller/skyline64:testbuild.20180221.rc0',
                'memory': 2048,
                'cpu': 1024,
                'logConfiguration': get_log_config()
            }
        ]
    }

    asg_name = LakituAwsConfig.windows_autoscale_name

    command_base = ['Ref::in', 'Ref::out', 'Ref::add_lib', '--add-library-name=encyclo_merged', '--save']

    def requires(self):
        return {'tool_wd': BuildToolDirectory(toolname='skyline'),
                'input_elib': self.clone(EncyclopediaBuildMergedLib),
                'sky_template_s3': UploadInputFileToS3(source_file=self.skyline_template,
                                                       sub_directory='skyline')}

    def run(self):
        # copy the input elib to the skyline directory first
        bname = os.path.basename(self.input()['input_elib'].path)
        s3helpers.cp_s3(self.input()['input_elib'].path, os.path.join(self.input()['tool_wd'].path, bname))
        self.run_ecs()

    @property
    def parameters(self):
        return {
            'in': '--in={}'.format(self.input()['sky_template_s3'].path),
            'out': '--out={}'.format(self.output()['skyline_out'].path),
            'add_lib': "--add-library-path={}".format(self.output()['copied_elib'].path)
        }

    def output(self):
        lib_basename = os.path.basename(self.input()['input_elib'].path)
        lib_out = os.path.join(self.input()['tool_wd'].path, lib_basename)
        return {'copied_elib': s3.S3Target(lib_out),
                'skyline_out': s3.S3Target(os.path.join(self.input()['tool_wd'].path, '0_sky_with_library.sky'))}

    @property
    def downloadable_outputs(self):
        # The intermediate .sky file isn't needed, but the final .sky file will refer to the copied elib
        return [self.output()['copied_elib']]


if __name__ == '__main__':
    luigi.run()
