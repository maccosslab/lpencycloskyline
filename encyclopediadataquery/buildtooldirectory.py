import os

import luigi
import luigi.contrib.s3 as s3
from lakitu.aws import s3helpers

from buildworkingdirectory import BuildWorkingDirectory


class BuildToolDirectory(luigi.Task):
    """
    Build a working directory for a tool. Sub directories can be defined using ::
    E.g. to put a tool directory for elib_boundaries under sub directory post processing
    pass toolname as postprocessing::elib_boundaries
    """
    toolname = luigi.Parameter()

    def requires(self):
        return {'base_directory': BuildWorkingDirectory()}

    def run(self):
        s3helpers.mkdir_s3(self.output().path)

    def output(self):
        base_path = self.input()['base_directory'].path
        return s3.S3Target(os.path.join(base_path, *(self.toolname.split('::'))))


if __name__ == '__main__':
    luigi.run()
