import posixpath

import luigi
from lakitu.aws.s3helpers import search_s3_prefix
from luigi.util import inherits

from copyalltooutputs3dir import CopyAllToOutputS3Dir
from encyclopediaparams import EncyclopediaParams
from importrawfilesskyline import ImportRawFilesSkyline
from util import S3PathParameter


@inherits(EncyclopediaParams)
class MainTask(luigi.WrapperTask):
    ms_set = S3PathParameter(description="Either"
                                         " (1) an S3 URL pointing to an S3 folder containing MS files,"
                                         " or (2) a comma-separated list of S3 URLs pointing to MS files")

    def requires(self):
        irf = self.clone(ImportRawFilesSkyline, ms_path_list=self._get_ms_paths())
        yield irf
        yield CopyAllToOutputS3Dir(seed_task=irf)

    def _get_ms_paths(self):
        formats = ("mzml", "mzxml", "mz5", "mgf", "text", "ms2", "cms2", "raw", "d", "wiff")

        # Handle s3 folder
        if self.ms_set.endswith('/'):
            # Get listing of s3 files in this directory
            return [s3_path for s3_path in search_s3_prefix(self.ms_set) if
                    posixpath.splitext(s3_path)[-1][1:].lower() in formats]
        # Handle comma-separated list of S3 MS file paths
        else:
            s3_paths = [list_item.strip() for list_item in self.ms_set.split(',')]
            return [s3_path for s3_path in s3_paths if posixpath.splitext(s3_path)[-1][1:].lower() in formats]


if __name__ == '__main__':
    luigi.run()
