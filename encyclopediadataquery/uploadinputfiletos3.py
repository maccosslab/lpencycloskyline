import os

import luigi
import luigi.contrib.s3 as s3
from lakitu.aws import s3helpers
from lakitu.aws.awstask import FilePathParameter

from buildworkingdirectory import BuildWorkingDirectory


class UploadInputFileToS3(luigi.Task):
    source_file = FilePathParameter(description="A local file path or an s3 path. If the file is an s3 path,"
                                                " it will be copied to the requested s3 location. If local, it is"
                                                " uploaded")
    sub_directory = luigi.Parameter(default="", description="Subdirectory within the working directory"
                                                            " use :: to delimit directories like this:"
                                                            " subdir::nestedsubdir")

    @property
    def _needs_upload(self):
        return not self.source_file.startswith('s3://')

    @property
    def resources(self):
        # allow only one large upload at a time per lakitu working directory
        stamp = self.requires()['s3_wd_location'].run_id
        return {"large_upload_{}".format(stamp): 1}

    def requires(self):
        return {"s3_wd_location": BuildWorkingDirectory()}

    def run(self):
        if self._needs_upload:
            s3helpers.upload_to_s3(self.source_file, self.output().path, show_progress=True)
        else:
            if self.source_file != self.output().path:
                s3helpers.cp_s3(self.source_file, self.output().path)

    def output(self):
        base_name = os.path.basename(self.source_file)
        out_path = [self.input()['s3_wd_location'].path]
        if len(self.sub_directory) > 0:
            out_path.extend(self.sub_directory.split('::'))
        out_path.append(base_name)
        return s3.S3Target(os.path.join(*out_path))


if __name__ == '__main__':
    luigi.run()
