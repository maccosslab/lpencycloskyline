import os

import luigi
import luigi.contrib.s3 as s3
from lakitu.aws import s3helpers
from lakitu.aws.awstask import FilePathParameter

from buildtooldirectory import BuildToolDirectory


class UploadMSConvertConfig(luigi.Task):
    msconvert_config = FilePathParameter(description="Path (s3 or local) to msconvert config")

    def requires(self):
        return {"msfiles_dir": BuildToolDirectory('msfiles')}

    def run(self):
        if not self.msconvert_config.startswith('s3://'):
            s3helpers.upload_to_s3(self.msconvert_config, self.output().path)

    def output(self):
        if self.msconvert_config.startswith('s3://'):
            return s3.S3Target(self.msconvert_config)
        return s3.S3Target(os.path.join(self.input()['msfiles_dir'].path, os.path.basename(self.msconvert_config)))


if __name__ == '__main__':
    luigi.run()
