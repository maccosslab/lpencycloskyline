import os
from hashlib import md5

import luigi
import luigi.contrib.s3 as s3
from lakitu.aws import s3helpers
from lakitu.aws.awstask import FilePathParameter
from lakitu.aws.ecs import ECSTask
from lakituapi.v0_0.pipeline import LakituAwsConfig
from luigi.util import inherits

from encyclopediabuildmergedlib import EncyclopediaBuildMergedLib
from msconvert import MSConvert, get_log_config
from util import remove_prefix
from addfastatoskyline import AddFastaToSkyline


@inherits(EncyclopediaBuildMergedLib)
class ImportFileSkyline(ECSTask):
    input_msfile_path = FilePathParameter(description="Input MS data file local or on s3")
    container_name = "skyline"
    cluster_name = LakituAwsConfig.windows_cluster_name
    task_def = {
        'family': 'skyline_testbuild_20180221_rc0_importfile',
        'taskRoleArn': LakituAwsConfig.task_arn,
        'containerDefinitions': [
            {
                'name': container_name,
                'image': 'atkeller/skyline64:testbuild.20180221.rc0',
                'memory': 4096,
                'cpu': 2048,
                'logConfiguration': get_log_config()
            }
        ]
    }

    asg_name = LakituAwsConfig.windows_autoscale_name
    command_base = ['Ref::in', 'Ref::ms_in', "--import-no-join"]

    def run(self):
        # run the job
        self.run_ecs()

        if self.skyd_path:
            # Add local path to msfile to success file. This full path is necessary for a hash for skyline import.
            s3helpers.touch_s3(self.output().path, contents=str(self.input()['msfile_s3'].path))

    def requires(self):
        return {'input_sky_s3': self.clone(AddFastaToSkyline),
                'msfile_s3': MSConvert(msc_in_path=self.input_msfile_path, msc_out_format='mzML')}

    @property
    def elib_path(self):
        return self.requires()['input_sky_s3'].elib_path

    @property
    def skyd_path(self):
        # look for the matching skyd output in the directory containing the input .sky file
        output_dir = os.path.dirname(self.input()['input_sky_s3'].path)
        # get the search prefix -- files in same directory as skyline input file, with same name as the input raw file
        # (not counting the extension). E.x. s3://edq_runs/skyline/inputraw
        raw_name, raw_ext = os.path.splitext(os.path.basename(self.input()['msfile_s3'].path))
        prefix = os.path.join(output_dir, raw_name)
        for path_match in s3helpers.search_s3_prefix(prefix):
            if path_match.endswith(raw_ext + '.skyd'):
                return path_match
        return ''

    @property
    def parameters(self):
        return {
            'in': '--in={}'.format(self.input()['input_sky_s3'].path),
            'ms_in': '--import-file={}'.format(self.input()['msfile_s3'].path)
        }

    @property
    def environment(self):
        # skyd gets output to teh same location as the skyline directory file.mzML -> file_numbertag.mzML.skyd or
        # file.mzML.skyd
        output_dir = os.path.dirname(self.input()['input_sky_s3'].path)
        ms_name, ms_extension = os.path.splitext(os.path.basename(self.input()['msfile_s3'].path))
        glob_base = ms_name + '*' + ms_extension + '.skyd'
        additional_uploads = remove_prefix(os.path.join(output_dir, glob_base), prefix='s3://')

        return [{'name': 's3wrap_extra_uploads', 'value': additional_uploads},
                {'name': 's3wrap_extra_downloads', 'value': remove_prefix(self.elib_path, prefix='s3://')}]

    @property
    def downloadable_outputs(self):
        # keep from downloading the flagged SUCCESS file
        return []

    def output(self):
        output_dir = os.path.dirname(self.input()['input_sky_s3'].path)
        # noinspection PyCallingNonCallable,PyDeprecation
        input_raw_hash = md5(self.input()['msfile_s3'].path).hexdigest()[0:8]
        success_path = os.path.join(output_dir,
                                    os.path.basename(self.input()['msfile_s3'].path) + '_SUCCESS_' + input_raw_hash)
        return s3.S3Target(success_path)


if __name__ == '__main__':
    luigi.run()
