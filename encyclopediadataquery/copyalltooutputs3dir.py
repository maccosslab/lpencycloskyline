import luigi
from lakitu.aws.ecs import ECSTask
from luigi.tools import deps

from copyoutputs import CopyOutputs
from util import SpecificTaskParameter


class CopyAllToOutputS3Dir(luigi.WrapperTask):
    seed_task = SpecificTaskParameter()

    def requires(self):
        upstream = deps.find_deps(self.seed_task, None)
        for t in upstream:
            if not isinstance(t, ECSTask):
                continue
            if len(t.downloadable_outputs) > 0:
                yield CopyOutputs(task_object=t)


if __name__ == '__main__':
    luigi.run()
