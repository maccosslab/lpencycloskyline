import os

import luigi
import luigi.contrib.s3 as s3
from lakitu.aws.ecs import ECSTask
from lakituapi.v0_0.pipeline import LakituAwsConfig
from luigi.util import inherits

from addlibrarytoskyline import AddLibraryToSkyline
from buildtooldirectory import BuildToolDirectory
from encyclopediabuildmergedlib import EncyclopediaBuildMergedLib
from msconvert import get_log_config
from uploadinputfiletos3 import UploadInputFileToS3
from util import remove_prefix


@inherits(EncyclopediaBuildMergedLib)
class AddFastaToSkyline(ECSTask):
    container_name = "skyline"
    cluster_name = LakituAwsConfig.windows_cluster_name
    task_def = {
        'family': 'skyline_testbuild_20180221_rc0_addfasta',
        'taskRoleArn': LakituAwsConfig.task_arn,
        'containerDefinitions': [
            {
                'name': container_name,
                'image': 'atkeller/skyline64:testbuild.20180221.rc0',
                'memory': 2048,
                'cpu': 2048,
                'logConfiguration': get_log_config()
            }
        ]
    }

    asg_name = LakituAwsConfig.windows_autoscale_name

    command_base = ['Ref::in', 'Ref::out', 'Ref::add_fasta', '--save']

    def requires(self):
        return {'tool_wd': BuildToolDirectory(toolname='skyline'),
                'input_sky_s3': self.clone(AddLibraryToSkyline),
                'fasta_s3': UploadInputFileToS3(source_file=self.fasta_file)}

    @property
    def elib_path(self):
        return self.input()['input_sky_s3']['copied_elib'].path

    @property
    def environment(self):
        additional_downloads = remove_prefix(self.elib_path, prefix='s3://')
        return [{'name': 's3wrap_extra_downloads', 'value': additional_downloads}]

    @property
    def parameters(self):
        return {
            'in': '--in={}'.format(self.input()['input_sky_s3']['skyline_out'].path),
            'out': '--out={}'.format(self.output().path),
            'add_fasta': "--import-fasta={}".format(self.input()['fasta_s3'].path)
        }

    def output(self):
        return s3.S3Target(os.path.join(self.input()['tool_wd'].path, '1_sky_with_fasta.sky'))

    @property
    def downloadable_outputs(self):
        # keep from downloading the intermediate .sky
        return []


if __name__ == '__main__':
    luigi.run()
