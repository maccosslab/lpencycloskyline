import os
import posixpath

import luigi
from lakitu.aws.awstask import FilePathParameter
from lakitu.aws.s3helpers import search_s3_prefix
from luigi.util import inherits

from copyalltooutputs3dir import CopyAllToOutputS3Dir
from downloadall import DownloadAll
from encyclopediaparams import EncyclopediaParams
from importrawfilesskyline import ImportRawFilesSkyline


@inherits(EncyclopediaParams)
class MainTask(luigi.WrapperTask):
    ms_set = FilePathParameter(description="Path to either "
                                           "(1) a local folder containing MS files, "
                                           "(2) a local file listing paths to local or S3 MS files, "
                                           "(3) an S3 folder containing MS files, "
                                           "or (4) a comma-separated list of S3 MS files")
    output_path = FilePathParameter(description="Path to download processing output to. If not specified, a default "
                                                "output S3 path of "
                                                "s3://<DataBucket>/<BuildWorkingDirectory-run-id>/output/ will be used "
                                                "to store the output",
                                    default=None)

    def requires(self):
        irf = self.clone(ImportRawFilesSkyline, ms_path_list=self._get_ms_paths())
        yield irf
        if self.output_path is None:
            yield CopyAllToOutputS3Dir(seed_task=irf)
        else:
            yield DownloadAll(seed_task=irf, output_path=self.output_path)

    def _get_ms_paths(self):
        formats = ("mzml", "mzxml", "mz5", "mgf", "text", "ms2", "cms2", "raw", "d", "wiff")

        # Handle S3 path
        if self.ms_set.startswith('s3://'):
            # Handle s3 folder
            if self.ms_set.endswith('/'):
                # Get listing of s3 files in this directory
                return [s3_path for s3_path in search_s3_prefix(self.ms_set) if
                        posixpath.splitext(s3_path)[-1][1:].lower() in formats]
            # Handle comma-separated list of S3 MS file paths
            else:
                s3_paths = [list_item.strip() for list_item in self.ms_set.split(',')]
                return [s3_path for s3_path in s3_paths if posixpath.splitext(s3_path)[-1][1:].lower() in formats]

        # We now expect a local path
        if not os.path.exists(self.ms_set):
            raise ValueError("ms file path {} does not exist".format(self.ms_set))
        if os.path.isdir(self.ms_set):
            return [os.path.abspath(os.path.join(self.ms_set, f))
                    for f in os.listdir(self.ms_set) if os.path.splitext(f)[-1][1:].lower() in formats]
        # path is a text file with an mzml file path on each line
        with open(self.ms_set) as fin:
            return [line.strip() for line in fin.readlines()]


if __name__ == '__main__':
    luigi.run()
