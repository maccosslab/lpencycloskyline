import os

import luigi
import luigi.contrib.s3 as s3
from lakitu.aws.awstask import FilePathParameter
from lakitu.aws.ecs import ECSTask
from lakituapi.v0_0.pipeline import LakituAwsConfig

from buildtooldirectory import BuildToolDirectory
from uploadmsconvertconfig import UploadMSConvertConfig
from uploadmsfiletos3 import UploadMsFileToS3


def get_log_config():
    """Get the logging configuration for an aws task definition"""
    return {'logDriver': 'awslogs',
            'options': {'awslogs-group': LakituAwsConfig.log_group_name,
                        'awslogs-region': LakituAwsConfig.log_group_region,
                        'awslogs-stream-prefix': "edq"}}


class MSConvert(ECSTask):
    download_converted = luigi.BoolParameter(significant=False, default=False, description="Download msconvert outputs"
                                                                                           "to client disk?")
    bypass_conversion = luigi.BoolParameter(default=True, description="If true, file conversion is bypassed if"
                                                                      "the input file extension matches the desired"
                                                                      " extension.")
    msc_in_path = FilePathParameter(description="S3 or local location for input file to convert")
    msc_out_format = luigi.ChoiceParameter(choices=['mzML', 'mzXML', 'mz5', 'mgf',
                                                    'text', 'ms1', 'cms1', 'ms2', 'cms2'],
                                           default='mzML',
                                           description="Desired output format for file, maps to "
                                                       "proteowizard types")

    asg_name = LakituAwsConfig.windows_autoscale_name
    msc_config = FilePathParameter(default=None,
                                   description="Path (s3 or local) to msconvert config.",
                                   always_in_help=True)
    container_name = "msconvert"
    cluster_name = LakituAwsConfig.windows_cluster_name
    task_def = {
        'family': 'msconvert',
        'taskRoleArn': LakituAwsConfig.task_arn,
        'volumes': [],
        'containerDefinitions': [
            {
                'name': container_name,
                'image': 'atkeller/msconvert:3.0.18114.070c9ea90',
                'memory': 2048,
                'cpu': 1024,
                'logConfiguration': get_log_config(),
            }
        ]
    }

    @property
    def _needs_convert(self):
        file_ext = os.path.splitext(self.msc_in_path)[-1][1:]
        return not (self.bypass_conversion and self.msc_out_format.lower() == file_ext.lower())

    def requires(self):
        reqs = {"msfiles_dir": BuildToolDirectory('msfiles'),
                "msc_in": UploadMsFileToS3(f_path=self.msc_in_path),
                "msconvert_dir": BuildToolDirectory(toolname="msconvert")}
        if self.msc_config is not None:
            reqs['msc_config_p'] = UploadMSConvertConfig(msconvert_config=self.msc_config)
        return reqs

    def run(self):
        if self._needs_convert:
            self.command_base = ['Ref::in_path', '-o', 'Ref::out_dir', '--outfile', 'Ref::out_path', 'Ref::extension']
            if self.msc_config is not None:
                # get the msconvert config
                self.command_base.extend(['-c', self.input()['msc_config_p'].path])
            self.run_ecs()

    @property
    def parameters(self):
        return {
            'in_path': self.input()['msc_in'].path,
            'out_dir': os.path.dirname(self.output().path),
            'out_path': self.output().path,
            'extension': '--' + self.msc_out_format
        }

    def output(self):
        if self._needs_convert:
            f_bname = os.path.basename(self.input()['msc_in'].path)
            f_out_name = os.path.splitext(f_bname)[0] + '.' + self.msc_out_format
            return s3.S3Target(os.path.join(self.input()['msconvert_dir'].path, f_out_name))
        else:
            return s3.S3Target(self.input()['msc_in'].path)

    @property
    def downloadable_outputs(self):
        if self.download_converted and self._needs_convert:
            return [self.output()]
        else:
            return []


if __name__ == '__main__':
    luigi.run()
