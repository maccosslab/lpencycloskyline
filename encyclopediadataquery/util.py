import ntpath
import os
import posixpath

import luigi
from luigi.parameter import ParameterException

try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse


def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text


class SpecificTaskParameter(luigi.Parameter):
    """
    Parameter corresponding to a specific instantiated task object
    """

    def serialize(self, x):
        return x.task_id


class S3Wrap(object):

    def __init__(self, os_name=os.name):
        self.os_name = os_name

    @property
    def os_path(self):
        if self.os_name == 'nt':
            return ntpath
        elif self.os_name == 'posix':
            return posixpath

    @property
    def scratch_dir(self):
        return os.environ.get('SCRATCH_DIR') or ('/tmp' if self.os_name == 'posix' else 'C:\\tmp')

    def create_tmp_from_key(self, key):
        """Create local temp file or folder depending on key path"""
        return self.os_path.join(self.scratch_dir, self.os_path.normpath(key))

    @staticmethod
    def path_to_bucket_and_key(path):
        (scheme, netloc, path, params, query, fragment) = urlparse(path)
        path_without_initial_slash = path[1:]
        return netloc, path_without_initial_slash

    def s3_to_tmp_directory(self, path):
        s3_loc = path.find("s3://")
        if s3_loc == -1:
            # there is no s3 path anywhere in this argument
            return path

        # the s3 path is from s3:// to the end of the argument
        s3_path = path[s3_loc:]
        src_bucket, src_key = S3Wrap.path_to_bucket_and_key(s3_path)
        local_tmp = self.create_tmp_from_key(src_key)

        return local_tmp


class S3PathParameter(luigi.Parameter):
    """
    Reads in and validates an s3 path
    """

    def parse(self, x):
        if x is None:
            return None
        if not x.startswith("s3://"):
            raise ParameterException("Path must be a valid s3 path or list of s3 paths")
        return x
