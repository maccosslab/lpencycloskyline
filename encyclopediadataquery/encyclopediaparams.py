import luigi
from lakitu.aws.awstask import FilePathParameter


class EncyclopediaParams(luigi.Config):
    precursor_ppm_error = luigi.FloatParameter(default=10.0,
                                               description='Encyclopedia: precursor ppm error')
    fragment_ppm_error = luigi.FloatParameter(default=10.0,
                                              description='Encyclopedia: fragment ppm error')
    query_elib_file = FilePathParameter(description='Encyclopedia elib library to query dia data with')
    fasta_file = FilePathParameter(description='FASTA file for EncyclopeDIA search')
