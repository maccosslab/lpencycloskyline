import os

import luigi
from lakitu.aws.awstask import FilePathParameter
from luigi.util import inherits
from walnutparams import WalnutParams
from walnutsinglelibrary import WalnutSingleLibrary
from downloadoutputs import DownloadOutputs


@inherits(WalnutParams)
class WalnutBatch(luigi.WrapperTask):
    ms_set = FilePathParameter(description="Path to a local folder containing MS files or a file listing "
                                           "paths to local or S3 MS files")
    output_path = FilePathParameter(description="Path to download processing output to")

    def requires(self):
        return [DownloadOutputs(task_object=WalnutSingleLibrary(ms_file=ms_path, fasta_file=self.fasta_file),
                                output_path=self.output_path) for ms_path in self._get_ms_paths()]

    def _get_ms_paths(self):
        if not os.path.exists(self.ms_set):
            raise ValueError("ms file path {} does not exist".format(self.ms_set))
        if os.path.isdir(self.ms_set):
            formats = ("mzml", "mzxml", "mz5", "mgf", "text", "ms2", "cms2", "raw", "d", "wiff")
            return [os.path.abspath(os.path.join(self.ms_set, f))
                    for f in os.listdir(self.ms_set) if os.path.splitext(f)[-1][1:].lower() in formats]
        # path is a text file with an mzml file path on each line
        with open(self.ms_set) as fin:
            return [line.strip() for line in fin.readlines()]


if __name__ == '__main__':
    luigi.run()
