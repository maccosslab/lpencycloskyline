import os

import luigi
from lakitu.aws import s3helpers
from lakitu.aws.awstask import FilePathParameter
from lakitu.util import fshelpers

from buildworkingdirectory import BuildWorkingDirectory
from util import SpecificTaskParameter


class DownloadOutputs(luigi.Task):
    task_object = SpecificTaskParameter()
    output_path = FilePathParameter()

    @property
    def resources(self):
        # allow only one download at a time per lakitu working directory
        stamp = self.requires()['base_directory'].run_id
        return {"download_{}".format(stamp): 1}

    def requires(self):
        return {'task': self.task_object,
                'base_directory': BuildWorkingDirectory()}

    def run(self):
        # do transfer to temp location and then move (use luigi thing)
        for s3_path, local_target in self.output().iteritems():
            local_path = local_target.path
            out_dir = os.path.dirname(local_path)
            if not os.path.exists(out_dir):
                os.makedirs(out_dir)
            with fshelpers.AtomicTempFile(local_path) as temp_output_path:
                s3helpers.download_from_s3(s3_path, temp_output_path, show_progress=False)

    def output(self):
        # map the s3 locations to local file paths, removing suffix of s3 base dir
        dl_requests = dict()
        for s3_target in self.requires()['task'].downloadable_outputs:
            s3_path = s3_target.path
            path_suffix = os.path.relpath(s3_path, start=self.input()['base_directory'].path)
            out_path = os.path.join(self.output_path, path_suffix)
            dl_requests[s3_path] = luigi.LocalTarget(out_path)
        return dl_requests


if __name__ == '__main__':
    luigi.run()
