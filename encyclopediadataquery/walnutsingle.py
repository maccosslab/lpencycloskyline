import os

import luigi
import luigi.contrib.s3 as s3
from lakitu.aws import s3helpers
from lakitu.aws.awstask import FilePathParameter
from lakitu.aws.batch import BatchTask
from lakituapi.v0_0.pipeline import LakituAwsConfig
from luigi.util import inherits

from buildtooldirectory import BuildToolDirectory
from buildworkingdirectory import BuildWorkingDirectory
from msconvert import MSConvert
from uploadinputfiletos3 import UploadInputFileToS3
from util import remove_prefix
from walnutparams import WalnutParams


@inherits(WalnutParams)
class WalnutSingle(BatchTask):

    @property
    def tool_wd(self):
        return self.input()['tool_wd'].path

    @property
    def fasta_in(self):
        return self.input()['fasta_s3'].path

    @property
    def mzml_in(self):
        return self.input()['mzml_s3'].path

    # BatchTask Interface
    job_queue_arn = LakituAwsConfig.linux_job_queue_arn
    job_def = {
        'jobDefinitionName': 'encyclopedia_0_6_3',
        'type': 'container',
        'containerProperties':
            {
                'image': 'atkeller/encyclopedia:0.6.3',
                'memory': 9000,
                'vcpus': 2  # allocate at least one core for this
            }
    }
    # /BatchTask Interface

    # AWSTaskBase Interface
    command_base = ['-walnut',
                    '-acquisition', 'DIA',
                    '-i', 'Ref::mzml_in',
                    '-f', 'Ref::fasta_in',
                    '-ptol', 'Ref::prec_ppm',
                    '-ftol', 'Ref::frag_ppm']

    @property
    def environment(self):
        additional_uploads = ""
        # get the mzml prefix: e.g. s3://bucket_name/mzml_dir/mzml_file.mzml -> bucket_name/mzml_dir/mzml_file
        mzml_prefix = remove_prefix(os.path.splitext(self.input()['mzml_s3'].path)[0], prefix='s3://')
        additional_uploads += mzml_prefix + '.dia'
        additional_uploads += ':' + mzml_prefix + '.mzML.pecan.txt.log'
        additional_uploads += ':' + mzml_prefix + '.mzML.features.txt'
        additional_uploads += ':' + mzml_prefix + '.mzML.pecan.txt'
        additional_uploads += ':' + mzml_prefix + '.mzML.pecan.decoy.txt'

        return [{'name': 's3wrap_extra_uploads', 'value': additional_uploads}]

    @property
    def parameters(self):
        return {
            'mzml_in': self.mzml_in,
            'fasta_in': self.fasta_in,
            'prec_ppm': str(self.precursor_ppm_error),
            'frag_ppm': str(self.fragment_ppm_error)
        }

    # /AWSTaskBase Interface

    # Luigi Interface
    ms_file = FilePathParameter()

    def requires(self):
        return {'s3_wd_location': BuildWorkingDirectory(),
                'tool_wd': BuildToolDirectory(toolname='walnut'),
                'fasta_s3': UploadInputFileToS3(source_file=self.fasta_file),
                'mzml_s3': MSConvert(msc_in_path=self.ms_file, msc_out_format='mzML')}

    def run(self):
        self.run_batch()
        # copy output from the input raw file directory to the correct output directory
        # try to get all files that share the prefix of the input file (with .mzml removed)
        # raw_s3 path: 's3://bucket-name/path/to/input_file.mzML
        # mzml_in_prefix: 's3://bucket-name/path/to/input_file'

        mzml_in_prefix = os.path.splitext(self.mzml_in)[0]

        # get objects with the same prefix key in this bucket
        for prefix_match in s3helpers.search_s3_prefix(mzml_in_prefix):
            # build the path where the file should be put
            mzml_path = os.path.dirname(mzml_in_prefix)
            match_suffix = prefix_match[len(mzml_path):]
            if match_suffix.startswith('/'):
                match_suffix = match_suffix[1:]
            output_path = os.path.join(self.output()['output_directory'].path, match_suffix)

            if prefix_match == self.mzml_in:
                # copy the input mzml
                print "Copying {} -> {}".format(prefix_match, output_path)
                s3helpers.cp_s3(prefix_match, output_path)
            else:
                print "Moving {} -> {}".format(prefix_match, output_path)
                s3helpers.mv_s3(prefix_match, output_path)

    def output(self):
        out_dir = self.tool_wd
        mzml_basename = os.path.basename(self.mzml_in)
        mzml_prefix = os.path.splitext(mzml_basename)[0]
        return {'output_directory': s3.S3Target(out_dir),
                'dia': s3.S3Target(os.path.join(out_dir, mzml_prefix + '.dia')),
                'pecan_txt_log': s3.S3Target(os.path.join(out_dir, mzml_basename + '.pecan.txt.log')),
                'pecan_txt': s3.S3Target(os.path.join(out_dir, mzml_basename + '.pecan.txt')),
                'pecan_decoy_txt': s3.S3Target(os.path.join(out_dir, mzml_basename + '.pecan.decoy.txt')),
                'features_txt': s3.S3Target(os.path.join(out_dir, mzml_basename + '.features.txt')),
                'copied_mzml': s3.S3Target(os.path.join(out_dir, mzml_basename))
                }
    # /Luigi Interface


if __name__ == '__main__':
    luigi.run()
