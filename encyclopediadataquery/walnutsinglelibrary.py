import os

import luigi
import luigi.contrib.s3 as s3
from lakitu.aws.awstask import FilePathParameter
from lakitu.aws.batch import BatchTask
from lakituapi.v0_0.pipeline import LakituAwsConfig
from luigi.util import inherits

from util import remove_prefix
from walnutparams import WalnutParams
from walnutsingle import WalnutSingle


@inherits(WalnutParams)
class WalnutSingleLibrary(BatchTask):

    @property
    def tool_wd(self):
        return self.requires()['walnut_run'].input()['tool_wd'].path

    @property
    def fasta_in(self):
        return self.requires()['walnut_run'].input()['fasta_s3'].path

    @property
    def mzml_in(self):
        return self.input()['walnut_run']['copied_mzml'].path

    @property
    def elib_out(self):
        return self.mzml_in + '.elib'

    # BatchTask Interface
    job_queue_arn = LakituAwsConfig.linux_job_queue_arn
    job_def = {
        'jobDefinitionName': 'encyclopedia_0_6_3',
        'type': 'container',
        'containerProperties':
            {
                'image': 'atkeller/encyclopedia:0.6.3',
                'memory': 9000,
                'vcpus': 2  # allocate at least one core for this
            }
    }
    # /BatchTask Interface

    # AWSTaskBase Interface
    command_base = ['-libexport',
                    '-pecan',
                    '-i', 'Ref::mzml_in',
                    '-o', 'Ref::elib_out',
                    '-f', 'Ref::fasta_in',
                    '-ptol', 'Ref::prec_ppm',
                    '-ftol', 'Ref::frag_ppm']

    @property
    def environment(self):
        # get the mzml prefix: e.g. s3://bucket_name/mzml_dir/mzml_file.mzml -> bucket_name/mzml_dir/mzml_file
        mzml_prefix = remove_prefix(os.path.splitext(self.mzml_in)[0], prefix='s3://')

        additional_downloads = ""
        additional_downloads += mzml_prefix + '.dia'
        additional_downloads += ':' + mzml_prefix + '.mzML.pecan.txt.log'
        additional_downloads += ':' + mzml_prefix + '.mzML.features.txt'
        additional_downloads += ':' + mzml_prefix + '.mzML.pecan.txt'
        additional_downloads += ':' + mzml_prefix + '.mzML.pecan.decoy.txt'

        additional_uploads = ""
        additional_uploads += mzml_prefix + '.mzML.elib'

        return [{'name': 's3wrap_extra_uploads', 'value': additional_uploads},
                {'name': 's3wrap_extra_downloads', 'value': additional_downloads}]

    @property
    def parameters(self):
        return {
            'mzml_in': self.mzml_in,
            'elib_out': self.elib_out,
            'fasta_in': self.fasta_in,
            'prec_ppm': str(self.precursor_ppm_error),
            'frag_ppm': str(self.fragment_ppm_error)
        }

    # /AWSTaskBase Interface

    # Luigi Interface
    ms_file = FilePathParameter()

    def requires(self):
        return {'walnut_run': WalnutSingle(ms_file=self.ms_file,
                                           fasta_file=self.fasta_file,
                                           fragment_ppm_error=self.fragment_ppm_error,
                                           precursor_ppm_error=self.precursor_ppm_error)}

    def output(self):
        out_dir = self.tool_wd
        mzml_basename = os.path.basename(self.mzml_in)
        mzml_prefix = os.path.splitext(mzml_basename)[0]
        return {'dia': s3.S3Target(os.path.join(out_dir, mzml_prefix + '.dia')),
                'pecan_txt_log': s3.S3Target(os.path.join(out_dir, mzml_basename + '.pecan.txt.log')),
                'pecan_txt': s3.S3Target(os.path.join(out_dir, mzml_basename + '.pecan.txt')),
                'pecan_decoy_txt': s3.S3Target(os.path.join(out_dir, mzml_basename + '.pecan.decoy.txt')),
                'features_txt': s3.S3Target(os.path.join(out_dir, mzml_basename + '.features.txt')),
                'copied_mzml': s3.S3Target(self.mzml_in),
                'elib': s3.S3Target(self.elib_out)
                }
    # /Luigi Interface


if __name__ == '__main__':
    luigi.run()
