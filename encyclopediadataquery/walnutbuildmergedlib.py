import os

import luigi
import luigi.contrib.s3 as s3
from lakitu.aws.batch import BatchTask
from lakituapi.v0_0.pipeline import LakituAwsConfig
from luigi.util import inherits

from encyclopediasingle import EncyclopediaSingle
from walnutparams import WalnutParams


@inherits(WalnutParams)
class WalnutBuildMergedLib(BatchTask):

    @property
    def lib_suffix(self):
        return '.blib' if self.blib == 'true' else '.elib'

    @property
    def query_elib_s3(self):
        # location on s3 of the query elib file
        return self.requires().values()[0].input()['library_s3'].path

    @property
    def query_fasta_s3(self):
        # location on s3 of the query fasta file
        return self.requires().values()[0].input()['fasta_s3'].path

    # BatchTask Interface
    job_queue_arn = LakituAwsConfig.linux_job_queue_arn
    job_def = {
        'jobDefinitionName': 'encyclopedia_0_6_3_mergelib',
        'type': 'container',
        'containerProperties':
            {
                'image': 'atkeller/encyclopedia:0.6.3',
                'memory': 9000,
                'vcpus': 2  # allocate two cores for merging
            }
    }
    # /BatchTask Interface

    # AWSTaskBase Interface
    command_base = ['Ref::memory',
                    '-libexport',
                    '-i', 'Ref::in_directory',
                    '-o', 'Ref::out_library',
                    '-l', 'Ref::query_elib',
                    '-f', 'Ref::query_fasta',
                    '-a', 'Ref::rt_align',
                    '-blib', 'Ref::blib']

    @property
    def parameters(self):
        return {
            'memory': '-Xmx6g',
            'in_directory': self.encyclopedia_s3_dir,
            'out_library': self.output().path,
            'query_elib': self.query_elib_s3,
            'query_fasta': self.query_fasta_s3,
            'rt_align': self.rt_align,
            'blib': self.blib
        }

    @property
    def encyclopedia_s3_dir(self):
        # directory with encyclopedia outputs
        e_path = self.input().values()[0]['output_directory'].path
        if not e_path.endswith('/'):
            e_path += '/'
        return e_path

    # /AWSTaskBase Interface

    # Luigi Interface
    # path to directory containing mzml files to analyze
    ms_path_list = luigi.ListParameter(description="A list of ms files to analyze on local system or S3")
    # perform rt alignment?
    rt_align = luigi.ChoiceParameter(choices=['true', 'false'], var_type=str, default='true',
                                     description='Encyclopedia: perform retention time alignment?')
    # output blib instead of elib?
    blib = luigi.ChoiceParameter(choices=['true', 'false'], var_type=str, default='false',
                                 description="If true, output blib file, if false (default) output elib")
    out_lib_name = luigi.Parameter(default='merged')

    def requires(self):
        # requires encyclopedia single run on all input mzml files
        return {ms_file: self.clone(EncyclopediaSingle, ms_file=ms_file)
                for ms_file in self.ms_path_list}

    def output(self):
        return s3.S3Target(os.path.join(self.encyclopedia_s3_dir, self.out_lib_name) + self.lib_suffix)
    # /Luigi Interface


if __name__ == '__main__':
    luigi.run()
