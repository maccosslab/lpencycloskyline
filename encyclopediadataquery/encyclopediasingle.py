import os

import luigi
import luigi.contrib.s3 as s3
from lakitu.aws import s3helpers
from lakitu.aws.awstask import FilePathParameter
from lakitu.aws.batch import BatchTask
from lakituapi.v0_0.pipeline import LakituAwsConfig
from luigi.util import inherits

from buildtooldirectory import BuildToolDirectory
from buildworkingdirectory import BuildWorkingDirectory
from encyclopediaparams import EncyclopediaParams
from msconvert import MSConvert
from uploadinputfiletos3 import UploadInputFileToS3
from util import remove_prefix


@inherits(EncyclopediaParams)
class EncyclopediaSingle(BatchTask):
    ms_file = FilePathParameter()
    job_queue_arn = LakituAwsConfig.linux_job_queue_arn
    job_def = {
        'jobDefinitionName': 'encyclopedia_0_6_3',
        'type': 'container',
        'containerProperties':
            {
                'image': 'atkeller/encyclopedia:0.6.3',
                'memory': 9000,
                'vcpus': 2  # allocate at least one core for this
            }
    }

    command_base = ['-acquisition', 'DIA',
                    '-i', 'Ref::mzml_in',
                    '-l', 'Ref::elib_in',
                    '-f', 'Ref::fasta_in',
                    '-ptol', 'Ref::prec_ppm',
                    '-ftol', 'Ref::frag_ppm']

    @property
    def environment(self):
        additional_uploads = ""
        # get the mzml prefix: e.g. s3://bucket_name/mzml_dir/mzml_file.mzml -> bucket_name/mzml_dir/mzml_file
        mzml_prefix = remove_prefix(os.path.splitext(self.input()['mzml_s3'].path)[0], prefix='s3://')
        additional_uploads += mzml_prefix + '.dia'
        additional_uploads += ':' + mzml_prefix + '.mzML.elib'
        additional_uploads += ':' + mzml_prefix + '.mzML.encyclopedia.txt'
        additional_uploads += ':' + mzml_prefix + '.mzML.encyclopedia.txt.delta_rt.pdf'
        additional_uploads += ':' + mzml_prefix + '.mzML.encyclopedia.txt.log'
        additional_uploads += ':' + mzml_prefix + '.mzML.encyclopedia.txt.rt_fit.pdf'
        additional_uploads += ':' + mzml_prefix + '.mzML.encyclopedia.txt.rt_fit.txt'
        additional_uploads += ':' + mzml_prefix + '.mzML.features.txt'
        additional_uploads += ':' + mzml_prefix + '.mzML.first_round.txt'

        return [{'name': 's3wrap_extra_uploads', 'value': additional_uploads},
                {'name': 'JAVA_OPTIONS', 'value': self.parameters['memory']}]

    @property
    def parameters(self):
        return {
            'memory': '-Xmx6g',
            'mzml_in': self.input()['mzml_s3'].path,
            'elib_in': self.input()['library_s3'].path,
            'fasta_in': self.input()['fasta_s3'].path,
            'prec_ppm': str(self.precursor_ppm_error),
            'frag_ppm': str(self.fragment_ppm_error)
        }

    def requires(self):
        return {'s3_wd_location': BuildWorkingDirectory(),
                'tool_wd': BuildToolDirectory(toolname='encyclopedia'),
                'library_s3': UploadInputFileToS3(source_file=self.query_elib_file),
                'fasta_s3': UploadInputFileToS3(source_file=self.fasta_file),
                'mzml_s3': MSConvert(msc_in_path=self.ms_file, msc_out_format='mzML')}

    def run(self):
        self.run_batch()
        # copy output from the input raw file directory to the correct output directory
        # try to get all files that share the prefix of the input file (with .mzml removed)
        # raw_s3 path: 's3://bucket-name/path/to/input_file.mzML
        # mzml_in_prefix: 's3://bucket-name/path/to/input_file'

        mzml_in_prefix = os.path.splitext(self.input()['mzml_s3'].path)[0]

        # get objects with the same prefix key in this bucket
        for prefix_match in s3helpers.search_s3_prefix(mzml_in_prefix):
            # build the path where the file should be put
            mzml_path = os.path.dirname(mzml_in_prefix)
            match_suffix = prefix_match[len(mzml_path):]
            if match_suffix.startswith('/'):
                match_suffix = match_suffix[1:]
            output_path = os.path.join(self.output()['output_directory'].path, match_suffix)

            if prefix_match == self.input()['mzml_s3'].path:
                # copy the input mzml
                print "Copying {} -> {}".format(prefix_match, output_path)
                s3helpers.cp_s3(prefix_match, output_path)
            else:
                print "Moving {} -> {}".format(prefix_match, output_path)
                s3helpers.mv_s3(prefix_match, output_path)

    def output(self):
        out_dir = self.input()['tool_wd'].path
        mzml_basename = os.path.basename(self.input()['mzml_s3'].path)
        mzml_prefix = os.path.splitext(mzml_basename)[0]
        return {'output_directory': s3.S3Target(out_dir),
                'elib': s3.S3Target(os.path.join(out_dir, mzml_basename + '.elib')),
                'dia': s3.S3Target(os.path.join(out_dir, mzml_prefix + '.dia')),
                'encyc_txt': s3.S3Target(os.path.join(out_dir, mzml_basename + '.encyclopedia.txt')),
                'features_txt': s3.S3Target(os.path.join(out_dir, mzml_basename + '.features.txt')),
                'copied_mzml': s3.S3Target(os.path.join(out_dir))
                }

    @property
    def downloadable_outputs(self):
        o = self.output()
        return [t for k, t in o.iteritems() if k not in ('output_directory', 'copied_mzml')]


if __name__ == '__main__':
    luigi.run()
