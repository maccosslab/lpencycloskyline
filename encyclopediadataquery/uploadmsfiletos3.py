import os

import luigi
import luigi.contrib.s3 as s3
from lakitu.aws import s3helpers
from lakitu.aws.awstask import FilePathParameter

from buildtooldirectory import BuildToolDirectory
from buildworkingdirectory import BuildWorkingDirectory


class UploadMsFileToS3(luigi.Task):
    f_path = FilePathParameter()

    @property
    def _needs_upload(self):
        return not self.f_path.startswith('s3://')

    @property
    def resources(self):
        if self._needs_upload:
            # allow only one raw upload at a time per lakitu working directory
            stamp = self.requires()['s3_wd_location'].run_id
            return {"large_upload_{}".format(stamp): 1}
        return {}

    def requires(self):
        return {"s3_wd_location": BuildWorkingDirectory(),
                "msfiles_dir": BuildToolDirectory(toolname="msfiles")}

    def run(self):
        if self._needs_upload:
            s3helpers.upload_to_s3(self.f_path, self.output().path, show_progress=True)

    def output(self):
        if not self._needs_upload:
            return s3.S3Target(self.f_path)
        raw_name = os.path.basename(self.f_path)
        file_subfolder = os.path.splitext(raw_name)[-1][1:].lower()
        return s3.S3Target(os.path.join(self.input()['msfiles_dir'].path, file_subfolder, raw_name))


if __name__ == '__main__':
    luigi.run()
