import os
from subprocess import check_output

from setuptools import setup, find_packages


def get_version_string():
    try:
        output = check_output(["git", "describe", "--tags"])
    except OSError as e:
        if e.errno == os.errno.ENOENT:
            # clarify file not found error.
            raise Exception("git must be in path to install lakituapi development version")
        else:
            # Something else went wrong while trying to run `check_output`
            raise
    parts = output.strip().split('-')
    if len(parts) == 1:
        return "{}".format(parts[0])
    else:
        tag, count, sha = parts[:3]
        return "{}.dev{}+{}".format(tag, count, sha)


setup(
    name="encyclopediadataquery",
    version=get_version_string(),
    description="A DIA data query pipeline",
    author="Jarrett Egertson",
    author_email="jegertso@uw.edu",
    classifiers=['Development Status :: 3 - Alpha',
                 'Programming Language :: Python :: 2.7'],
    packages=find_packages(),
    install_requires=['luigi', 'boto3', 'lakituapi', 'lakitu', 'boto']
)
